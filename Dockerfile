FROM python:3.9-slim-buster

LABEL maintainer="Nicolas Schuler"
LABEL email="n-schuler@gmx.de"
LABEL description="A pep dummy"

RUN adduser --disabled-password \
    --gecos "" \
    dummy
WORKDIR /home/dummy/src
COPY . .
RUN pip install -r requirements.txt
CMD python main.py
