import os

from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    pod_name: str = "PEP"
    pod_namespace: str
    pod_uid: str
    pod_ip: str
    node_name: str
    node_ip: str
    pdp_port: int = 80
    pip_port: int = 80

    @property
    def pdp_address(self) -> str:
        return (
            os.environ.get("PDP_ADRESS")
            or f"ws://pdp-service.{self.pod_namespace}:{self.pdp_port}/ws"
        )

    @property
    def pip_address(self) -> str:
        return (
            os.environ.get("PIP_ADRESS")
            or f"ws://pip-service.{self.pod_namespace}:{self.pip_port}/ws"
        )


@lru_cache()
def get_settings():
    return Settings()
