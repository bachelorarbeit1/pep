# Policy Execution Point (PEP)

This is a repository of a Policy Execution Point (PEP) dummy writen in Python.

## Usage
The pep dummy will try to connect to a pdp and pip over a websocket connection. In order for this to work a few environment variables need to be set up beforehand. This repository assumes a deployment via Kubernetes. A lot of the listed environment variables can be easily injected by Kubernetes via the downward-api.

| Name | Description |
| ---      |  ------  |
| POD_NAME   | name of the pod running the pep  |
| POD_NAMESPACE | namespace of the pod running the pep |
| POD_UID | an unique id of the pod running the pep |
| POD_IP | the ip of the pod running the pep |
| NODE_NAME | name of the node running the pod |
| NODE_IP | ip of the node running the pod|
| PDP_PORT | port of the pdp you wish to connect to via websocket. If omitted the PDP_ADRESS variable is used. This variable has a lower priority than PDP_ADRESS. The resulting path will be: `ws://pdp-service.$POD_NAMESPACE:$PDP_PORT/ws` |
| PIP_PORT | port of the pip you wish to connect to via websocket. If omitted the PIP_ADRESS variable is used. This variable has a lower priority than PIP_ADRESS. The resulting path will be: `ws://pip-service.$POD_NAMESPACE:$PDP_PORT/ws` |
| PDP_ADRESS | adress of the pdp wih the correct port and route for establishing a websocket connection. This variable takes precedence over the PDP_PORT. |
| PIP_ADRESS | adress of the pdp with the correct port and route for establishing a websocket connection. This variable takes precedence over the PIP_PORT |

### Run the docker image

For running the Docker image you need to pass in the environment variables. This can be done via:

`-e` Parameter in the docker run command:

`docker run -e POD_NAME='pep' -e <...> --name pep nschuler/thesis:pep`

an env-file:

`docker run --env-file ./.env --name pep nschuler/thesis:pep`

## Communication
```mermaid
graph LR
    PEP(PEP) -- websocket ---PDP(PDP)
    PEP(PEP) -- websocket ---PIP(PIP)
```
### Sequence Diagram
```mermaid
sequenceDiagram
    par Communication with PDP
    PEP->>PDP: 'Hello I'm a PEP'
    loop True
            PEP->>PDP: {'name': 'pep', 'namespace': 'division-1', 'uid': '12uid', <br>'pod_ip': '10.0.0.1', 'node_name': 'division-1-n1', 'node-ip': '134.12.12.13' }
            PDP-->>PEP: 'Message received'
    end
    and Communication with PIP
        PEP->>PIP: 'Hello I'm a PEP'
    loop True
            PEP->>PIP: {'name': 'pep', 'namespace': 'division-1', 'uid': '12uid', <br>'pod_ip': '10.0.0.1', 'node_name': 'division-1-n1', 'node-ip': '134.12.12.13' }
            PIP-->>PEP: 'Message received'
    end
    end
```
**The par box means parallel execution. However, in our case the communication happens asynchronously**

## Other
This Repository uses the gitlab-ci to:
- check compliance with the black code style
- create and deploy a docker image to dockerhub

If your planning to clone the repository you need to set the following Variables in order to get the CI working correctly:

| Name | Description |
| ---      |  ------  |
| CI_REGISTRY_TOKEN   | registry Token from dockerhub for pushing the image |
| DOCKER_ACCOUNT | name of the Account |
| DOCKER_IMAGE | name of the image |
| DOCKER_TAG | tag for the image |

**Do not forget to mask the CI_REGISTRY_TOKEN or everyone could push images to your account by looking at the CI logs**
