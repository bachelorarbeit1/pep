import asyncio
import time
import websockets
import logging

from app import config
from app.models import Event, Origin

servers = set()
servers.add(config.get_settings().pdp_address)
servers.add(config.get_settings().pip_address)


async def handle_websockets(uri: str):
    async with websockets.connect(uri) as websocket:
        logging.info(f"Sending infos about pep to {uri}")
        await websocket.send(f"Hello I'm a PEP")
        msg = Origin(
            name=config.get_settings().pod_name,
            namespace=config.get_settings().pod_namespace,
            uid=config.get_settings().pod_uid,
            pod_ip=config.get_settings().pod_ip,
            node_name=config.get_settings().node_name,
            node_ip=config.get_settings().node_ip,
        )
        await websocket.send(msg.json())
        logging.info(f"Waiting for answer from {uri}")
        await websocket.recv()
        logging.info(f"Answer from {uri} received")
        await run_in_loop(websocket, uri, msg)


async def run_in_loop(ws, uri: str, origin: Origin):
    while True:
        logging.info(f"Sending event to {uri}")
        msg = Event(origin=origin, target=uri, message="Event propagation")
        await ws.send(msg.json())
        logging.info(f"Waiting for answer from {uri}")
        await ws.recv()
        logging.info(f"Answer from {uri} received")
        time.sleep(1)


async def main():
    await asyncio.wait([handle_websockets(uri) for uri in servers])


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s — %(name)s — %(levelname)s — %(message)s",
    )
    logging.info("Application start")
    asyncio.get_event_loop().run_until_complete(main())
    logging.info("Application terminated")
